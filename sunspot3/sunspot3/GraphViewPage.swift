//
//  LiveViewPage.swift
//  sunspot3
//
//  Created by Janek Mann on 25/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class GraphViewPage: BasePageViewController, UVDataManagerDelegate {
    // locationHanlder object to handle GPS data
    
    @IBOutlet var dailyGraph: DailyGraphView!
    override func viewDidLoad() {
        super.viewDidLoad()
        dailyGraph.values = [0.0,0.0,0.0,0.0,0.0]
        self.dailyGraph.values = WeatherService.sharedInstance.values
        self.dailyGraph.xIndices = WeatherService.sharedInstance.xIndices
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.dailyGraph.values = WeatherService.sharedInstance.values
        self.dailyGraph.xIndices = WeatherService.sharedInstance.xIndices
    }
    func liveData(uvIndex: Float, lux: Float) {
        
    }
}