//
//  WeatherPrediction.swift
//  sunspot3
//
//  Created by Paolo Di Prodi on 23/09/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire
import SwiftyJSON

public enum Status {
    case success
    case failure
}

private let _weatherServiceSharedInstance = WeatherService()

public class ResponseUV {
    public var status: Status = .failure
    public var yvals: Array<Double>? = nil
    public var xvals: Array<String>? = nil
    public var error: ErrorType? = nil
    public var resolution : String? = nil
}

public class WeatherService: LocationHandlerProtocol {
    let locationHandler = LocationHandler()
    var lastPredictionDate: NSDate = NSDate(timeIntervalSince1970: 0)
    private var key: String
    var xIndices: [String] = ["Today"]
    var values: [Double] = [0.0]
    public init() {
        self.key = WorldWeatherOnline.sharedInstance.getKey()!
    }
    
    public func start_prediction() {
        print ("Weather Prediction requested")
        locationHandler.startLocationTracking()
        locationHandler.locationHandlerProtocol = self
        locationHandler.requestLocation()

    }
    
    
    func updateDailyPrediction(origin:CLLocation)
    {
        //do the update here
        
        self.retrieveForecast(origin,
            success: { response in
                //[TODO]: let days = getDayOfWeek(response.xvals!)
                print ("UV prediction: \(response.yvals!)")
                print ("Dates: \(response.xvals)")
                self.xIndices = response.xvals.map({getDayOfWeek($0)!})!
                self.values = response.yvals!
                
            }, failure:{ response in
                print("Error: \(response.error)")
        })
        
    }
    @objc func locationHandlerDidUpdateLocation(location: CLLocation) {
        print("New location acquired")
        print(location)
        if (lastPredictionDate.timeIntervalSinceNow < -3600) {
            lastPredictionDate = NSDate(timeIntervalSinceNow: 0)
            updateDailyPrediction(location)
        }
        
    }
    @objc func locationHandlerDidUpdateLocationWithSpeed(speed: Double, location: CLLocation) {
        print("Location + speed")
        print(location)
        
    }
    class var sharedInstance: WeatherService {
        return _weatherServiceSharedInstance
    }
    public func extractUV(data:JSON)->(x:Array<String>,y:Array<Double>)
    {
        // Get forecast
        var uvdata = Array<Double>()
        var dates = Array<String>()
        
        for index in 0...data["data"]["weather"].count-1 {
            let temp = data["data"]["weather"][index]["uvIndex"].doubleValue
            if (temp <= 0.0)
            {    uvdata.append(0)}
            else
            {    uvdata.append(temp)}
            
            dates.append(data["data"]["weather"][index]["date"].stringValue)
        }
        return (dates,uvdata)
        
    }
    public func retrieveForecast(location: CLLocation, success:(response: ResponseUV)->(), failure: (response:ResponseUV)->()){
        
        let url = "http://api.worldweatheronline.com/premium/v1/weather.ashx"
        let params = ["q":"\(location.coordinate.latitude),\(location.coordinate.longitude)", "key":self.key, "format":"json"]
        print(params)
        print(url)
        
        Alamofire.request(.GET, url, parameters: params)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    print("Validation Successful")
                    if let jweather:JSON = JSON(data : response.data!)  {
                        let out = ResponseUV()
                        out.status = .success
                        let parsed = self.extractUV(jweather)
                        out.yvals = parsed.y
                        out.xvals = parsed.x
                        out.resolution = "day"
                        success(response: out)
                    }

                case .Failure(let error):
                    let out = ResponseUV()
                    out.status = .failure
                    out.error = error
                    failure(response: out)
                }
        }
        
    }
}

