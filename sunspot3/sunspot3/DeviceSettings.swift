//
//  DeviceSettings.swift
//  sunspot
//
//  Created by Paolo Di Prodi on 01/05/2015.
//  Copyright (c) 2015 spotsensors. All rights reserved.
//

import Foundation

import Foundation

private let _DeviceSettingSharedInstance = DeviceSetting()


class DeviceSetting
{
    var defaults: NSUserDefaults?
    var nsuuid:String?

    class var sharedInstance: DeviceSetting {
        return _DeviceSettingSharedInstance
    }
    
    init() {
        print("Device setting initiated")
        defaults = NSUserDefaults.standardUserDefaults()
    }
    
    func saveNSUUID(text:String) {
        defaults?.setObject(text, forKey: "nsuuid")
        nsuuid = text
    }
    
    func getNSUUID()->String? {
        if let nsuuid:String = defaults?.stringForKey("nsuuid")
        {
            return nsuuid
        }
        else
        {
            return self.nsuuid
        }
        
    }

}