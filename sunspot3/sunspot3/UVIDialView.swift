//
//  UVIDialView.swift
//  sunspot3
//
//  Created by Janek Mann on 26/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore


@IBDesignable
class uVIDialView: UIView {
    let pulseSpeed = 6.0
    @IBInspectable var uVI: CGFloat = 0.0 {
        // Update your UI when value changes
        didSet {
            self.pulseActive = true
            self.refTimestamp = updater.timestamp
            NSTimer.scheduledTimerWithTimeInterval((M_PI * 2.0) / pulseSpeed, target: self, selector: "pulseTimeOut", userInfo: nil, repeats: false)
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var pulseAlpha: Double = 0.0 {
        // Update your UI when value changes
        didSet {
            self.setNeedsDisplay()
        }
    }
    var pulseActive: Bool = false
    var updater: CADisplayLink!
    var refTimestamp = 0.0
    func pulseTimeOut() {
        self.pulseActive = false
        self.pulseAlpha = 0.0
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        //updater = CADisplayLink(target: self, selector: Selector("animateView"))
        //updater.frameInterval = 1
        //updater.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSRunLoopCommonModes)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updater = CADisplayLink(target: self, selector: Selector("animateView"))
        updater.frameInterval = 2
        updater.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSRunLoopCommonModes)
        //super.init(aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    func animateView() {
        if (self.pulseActive) {
            pulseAlpha = sin((updater.timestamp - refTimestamp) * pulseSpeed - (M_PI / 2.0)) * 0.4 + 0.4
            self.setNeedsDisplay()
        }
    }
    override func drawRect(rect: CGRect) {
        let frame1 = self.frame.insetBy(dx: 0, dy: 0)
        let context = UIGraphicsGetCurrentContext()
        CGContextSaveGState(context)
        CGContextScaleCTM (context, frame1.width/240, frame1.height/240)
        //// Oval Drawing
        
        CGContextSaveGState(context)
        CGContextSetAlpha(context, CGFloat(pulseAlpha))
        CGContextDrawRadialGradient(context, SunSpotStyleKit.graphGradient, CGPointMake(120, 120), 90, CGPointMake(120, 120), 0, CGGradientDrawingOptions(rawValue: 0))

        CGContextRestoreGState(context)

        SunSpotStyleKit.drawDial(CGRect(x: 0, y: 0, width: 240, height: 240), uvi: uVI)

        CGContextRestoreGState(context)
    }
}