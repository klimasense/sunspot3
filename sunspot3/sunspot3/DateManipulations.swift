//
//  DateManipulations.swift
//  SunSpot
//
//  Created by Paolo Di Prodi on 11/07/2015.
//

import Foundation


func getDayOfWeek(today:String)->String? {
    let names:Array<String> = ["Today","Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
    let formatter  = NSDateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    if let todayDate = formatter.dateFromString(today) {
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components(NSCalendarUnit.Weekday, fromDate: todayDate)
        let weekDay = myComponents.weekday
        return names[weekDay]
    } else {
        return nil
    }
}

func getDayOfWeek(dates:Array<String>)->Array<String>? {
    
    var days = Array<String>()
    
    for date in dates{
        days.append(getDayOfWeek(date)!)
        
    }
    return days
    
}

func getCurrentHour()->(hour:Int,minute:Int)
{
    let date = NSDate()
    let calendar = NSCalendar.currentCalendar()
    let components = calendar.components(NSCalendarUnit.Hour, fromDate: date)
    let hour = components.hour
    let components2 = calendar.components(NSCalendarUnit.Minute, fromDate: date)
    let minutes = components2.minute
    
    return (hour,minutes)
    
}

func timeDiff(date1:NSDate,date2:NSDate)->(hour:Int,minute:Int)
{
    // Make a Gregorian calendar
    let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)
    
    let durationDateComponents = calendar?.components(NSCalendarUnit.Day, fromDate: date1, toDate: date2, options: NSCalendarOptions())
    
    let hours = durationDateComponents?.hour
    let minutes = durationDateComponents?.minute
    return (hours!,minutes!)
}

extension NSDate
{
    func hour() -> Int
    {
        //Get Hour
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(NSCalendarUnit.Hour, fromDate: self)
        let hour = components.hour
        
        //Return Hour
        return hour
    }
    
    
    func minute() -> Int
    {
        //Get Minute
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(NSCalendarUnit.Minute, fromDate: self)
        let minute = components.minute
        
        //Return Minute
        return minute
    }
    
    func toShortTimeString() -> String
    {
        //Get Short Time String
        let formatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        let timeString = formatter.stringFromDate(self)
        
        //Return Short Time String
        return timeString
    }
}
