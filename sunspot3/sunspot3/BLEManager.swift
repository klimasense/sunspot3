//
//  BLEManager.swift
//  sunspot
//
//  Created by Janek Mann on 22/03/2015.
//  Copyright (c) 2015 spotsensors. All rights reserved.
//

import Foundation
import CoreBluetooth

var btManager:CBCentralManager? = nil
var loggingEnabled = true

let SunSpotCharacteristicUUID = CBUUID(string: "2A37")
let SunSpotServiceUUID = CBUUID(string: "180D")
let dataManager = UVDataManager.sharedInstance
let rssiManager = ProximityDataManager.sharedInstance
let IR_dark:Float = 25.3;
let VIS_dark:Float = 26.0;

class BLEManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate
{
    var peripheral: CBPeripheral?
    var devicesSeen: [(Int, CBPeripheral)] = []
    
    var timerRSSI : NSTimer?
    
    override init() {
        super.init();
        
        print("Test")
    }
    
    func start() {
        print("ble start")
        if (btManager == nil) {
            btManager = CBCentralManager(delegate: self, queue: nil)
            
        }
        
    }
    func RssiUpdate()
    {
        if (self.peripheral != nil) {
            //read the signal strength
            self.peripheral!.readRSSI()
        }
        
    }
    
    func centralManagerDidUpdateState(central: CBCentralManager) {
        switch (central.state)
        {
        case .Unsupported:
            
            break
        case .Unauthorized:
            
            break
        case .PoweredOff:
            
            break
        case .PoweredOn:
            restoreConnection()
            break
        case .Unknown:
            break
        default:
            break
        }
    }
    func pairingTimeout() {
        print(devicesSeen)
        func sortfunc(value0: (Int, CBPeripheral), value1: (Int, CBPeripheral)) -> Bool {
            return (value0.0 > value1.0)
        }
        
        if (devicesSeen.count>0)
        {
        //self.peripheral = peripheral
        self.peripheral = devicesSeen.sort(sortfunc)[0].1
        print("Connecting to \(self.peripheral!.name)")
        btManager!.connectPeripheral(self.peripheral!, options: nil)
        //saving into settings
        UserSetting.sharedInstance.saveDevice(self.peripheral?.name)
        } else {
            startPairing()
        }
    }
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        
        //println("detected peripheral: \(peripheral)")
        //print(UserSetting.sharedInstance.getDevice())
        //print("peripheral.name: \(peripheral.name)")
        if let name = peripheral.name {
            //TODO: my sunspot3 doesn't work so have to use this trick
            // alternative: not case sensitive
            print ("device id: \(peripheral.identifier), saved ID: \(DeviceSetting.sharedInstance.getNSUUID())")
            if let deviceId = DeviceSetting.sharedInstance.getNSUUID() {
                if (peripheral.identifier.UUIDString == deviceId) {
                    print ("found previously paired device, cancelling pairing")
                    btManager!.connectPeripheral(peripheral, options: nil)
                    NSTimer.cancelPreviousPerformRequestsWithTarget("pairingTimeout")
                }
            }
            if name.lowercaseString.rangeOfString("sun") != nil {
                print("Found potential sensor \(name)")
                devicesSeen.append((Int(RSSI), peripheral))
            }
            
            /*
            if name == UserSetting.sharedInstance.getDevice() {
                devicesSeen.append((Int(RSSI), peripheral))
            }
            */
        }
        
    }
    func startPairing() {
        if let peripheral = self.peripheral {
            DeviceSetting.sharedInstance.saveNSUUID("")
            btManager!.cancelPeripheralConnection(peripheral)
        } else {
            btManager!.scanForPeripheralsWithServices(nil, options:nil)
            NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "pairingTimeout", userInfo: nil, repeats: false)
        }
    }
    
    func restoreConnection() {
        if let peripheral = self.peripheral
        {
            print("Reconnecting to sunspot...")
            btManager!.connectPeripheral(peripheral, options: nil)
        }
        else
        {
            startPairing()
        }
        
    }
    
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        self.timerRSSI?.invalidate()
        print("\(self.peripheral?.name) disconnected")
        rssiManager.disconnected()
        restoreConnection()
    }
    
    func centralManager(central: CBCentralManager!, didRetrievePeripherals peripherals: [AnyObject]!) {
        print("Retrieve peripherals .....")
    }
    
    func centralManager(central: CBCentralManager!, didRetrievePeripheralsWithIdentifiers peripherals: [AnyObject]!) {
        
        print(peripherals)
        if peripherals.count > 0 {
            
            self.peripheral = peripherals[0] as? CBPeripheral
            
            print("Current peripheral \(self.peripheral)")
            self.peripheral!.delegate = self
            btManager!.connectPeripheral(self.peripheral!, options: nil)
        }
    }
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        if loggingEnabled {print("Connected to: \(peripheral.name)")}

        peripheral.delegate = self
        peripheral.discoverServices(nil)
        //activate the timer
        self.timerRSSI = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("RssiUpdate"), userInfo: nil, repeats: true);
    }
    
    func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        if loggingEnabled {print("Failed to connect")}
        self.startPairing()
        
    }
    
    
    //MARK: CBPeripheralDelegate methods
    
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        
        for aService in peripheral.services! {
            
            let cbService = aService 
            print("\(cbService)")
            /* Orient Service */
            if cbService.UUID == SunSpotServiceUUID {
                print("found service")
                peripheral.discoverCharacteristics(nil, forService: cbService)

            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        for aCharacteristic  in service.characteristics! {
            let cbCharacteristic = aCharacteristic 
            if cbCharacteristic.UUID == SunSpotCharacteristicUUID {
                print("Found characteristic")
                self.peripheral?.setNotifyValue(true, forCharacteristic: cbCharacteristic)
                //save the NSUUID since we have found the right characteristic in the device
                let peripheral_id: NSUUID = peripheral.identifier
                DeviceSetting.sharedInstance.saveNSUUID(peripheral_id.UUIDString)
                if loggingEnabled {print("saved peripheral id: \(peripheral_id.UUIDString)")}
            }
        }
        
    }

    func peripheralDidUpdateRSSI(peripheral: CBPeripheral, error: NSError?) {

        if let rssi:Int = peripheral.RSSI!.integerValue
        {
        rssiManager.updateLiveData(rssi)
        //print("Did update RSSI: \(rssi)")
        }
    }
    func floatFromData(data: NSData, range: NSRange) -> Float {
        var rawVal : UInt16 = 0
        data.getBytes(&rawVal, range: range)
        let signedVal = Int16(bitPattern: CFSwapInt16LittleToHost(rawVal))
        let divisor = 140
        return (Float(signedVal) / Float(divisor))
        
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        let value = characteristic.value
        //println("Got data: \(value)")
        
        let uvi = floatFromData(value!, range: NSRange(location: 14, length: 2))
        let lux = Float(0.0)
        //let lux = (floatFromData(value!, range: NSRange(location: 12, length: 2)) - VIS_dark) * 800.0 // * 5.41 * 14.5
        dataManager.updateLiveData(uvi, lux: lux)
        
    }
    
}