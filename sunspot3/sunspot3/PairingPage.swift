//
//  PairingPage.swift
//  sunspot3
//
//  Created by Janek Mann on 25/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit

class PairingPage: BasePageViewController, UVDataManagerDelegate  {
    @IBOutlet var instruction: UILabel!
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {})
    }
    func pairingTimeout() {
        self.dismissViewControllerAnimated(true, completion: {})
    }
    @IBAction func pairButtonPressed(sender: AnyObject) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.bleManager.startPairing()
        instruction.text = "Pairing... Please wait."
        
        NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: "pairingTimeout", userInfo: nil, repeats: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //check if we have already paired
        if let devicename = UserSetting.sharedInstance.getDevice()
        {
            instruction.text = "Paired to \(devicename), please place your SunSpot next to the phone to re-pair"
        }
        else
        {
            instruction.text = "Please place your SunSpot next to the phone to pair"
            
        }
        
    }
    func liveData(uvIndex: Float, lux: Float) {
        
    }
}