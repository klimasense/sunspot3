//
//  Bucketize.swift
//  sunspot
//
//  Created by Paolo Di Prodi on 22/03/2015.
//  Copyright (c) 2015 spotsensors. All rights reserved.
//

import Foundation

//internally intervals are expressed in seconds
public class BucketizeDay {
    
    //min and max bucket index
    var minimum: Int
    var maximum: Int
    //store the current date for this object
    var currentDate:String
    //24 hour histogram keeping max uv for each hour
    var buckets: [Double]
    //number of bins
    var bins:Int
    var defaults: NSUserDefaults?
    var dateFormatter = NSDateFormatter()
    
    //initialize the histogram array of counts with at least 24 hours time span
    // the structure is like bucket[hour]=max(UVindex)
    init(samplesinday:Int=24)
    {
        
        self.minimum = 0
        //maximum seconds in a day
        self.maximum = 24*60*60;
        //number of bins is either 24 or 48 (one sample/two sample per hour)
        self.bins=samplesinday
        
        
        dateFormatter.dateFormat = "yyyy-MM-dd" //format style. Browse online to get a format that fits your needs.
        self.currentDate = dateFormatter.stringFromDate(NSDate())
        
        buckets = Array(count: bins, repeatedValue: 0.0)
        defaults = NSUserDefaults.standardUserDefaults()
        //safe the initial date
        defaults?.setObject(buckets, forKey:self.currentDate)
    }
    
    func getBuckets() -> ([Double])
    {
        return buckets
    }
    
    func GetHour() -> Int
    {
        var hour:Int
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(NSCalendarUnit.Hour , fromDate: date)
        hour = components.hour
        return hour
        
    }
    func GetValue(index:Int)->Double{
        return self.buckets[index]
        
    }
    func Bucketize(tstamp:NSDate?,value:Double) -> (index:Int,value:Double)
    {
        //first check if we are on a different date
        if (self.currentDate != dateFormatter.stringFromDate(tstamp!))
        {
            //save the old bucket and create a new one
            defaults?.setObject(buckets, forKey:self.currentDate)
            self.currentDate = dateFormatter.stringFromDate(tstamp!)
            //save the new one with all zeros
            defaults?.setObject(buckets, forKey:self.currentDate)
        }
        //find the hour minute and seconds in the current day
        var hour:Int
        var minutes:Int
        
        if tstamp==nil{
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(NSCalendarUnit.Hour , fromDate: date)
            hour = components.hour
            minutes = components.minute
            
        }
        else
        {
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(NSCalendarUnit.Hour, fromDate: tstamp!)
            hour = components.hour
            let components2 = calendar.components(NSCalendarUnit.Minute, fromDate: tstamp!)
            minutes = components2.minute
            
        }
        //here should combine hour and minutes according to 24 or 48 hours
        let dayseconds:Int = hour*60*60+minutes*60;
        
        
        let bucketSize:Int = (maximum - minimum)/buckets.count;
        var bucketIndex:Int = (dayseconds-minimum)/bucketSize;
        if bucketIndex==buckets.count {
            bucketIndex -= 1
        }
        //take the maximum between the previous value and the current one
        let oldValue = buckets[bucketIndex]
        let newValue = max(oldValue ,value)
        buckets[bucketIndex] = max(oldValue ,value)
        //time to save
        if (oldValue != value)
        {
            //safe the initial date
            defaults?.setObject(buckets, forKey:self.currentDate)
            
        }
        return (bucketIndex,newValue)
        
    }
    
}