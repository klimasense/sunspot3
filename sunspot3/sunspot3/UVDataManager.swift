//
//  UVDataManager.swift
//  sunspot
//
//  Created by Janek Mann on 18/03/2015.
//  Copyright (c) 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit

private let _UVDataManagerSharedInstance = UVDataManager()
private let _ProximityDataManagerSharedInstance = ProximityDataManager()

protocol UVDataManagerDelegate {
    func liveData(uvIndex: Float, lux: Float)
}
protocol ProximityDataManagerDelegate {
    func liveData(rssi: Int)
    func disconnected()
}


class UVDataManager
{

    var delegate:UVDataManagerDelegate?
    var last_uv_alert: Float?
    var last_time_alert: NSDate?
    var last_uv: Float?
    var telemetry : InitialState
    
    class var sharedInstance: UVDataManager {
        return _UVDataManagerSharedInstance
    }
    
    init() {
        print("UV Data Manager created")
        last_time_alert = NSDate()
        last_uv_alert = 0.0
        last_uv = 0.0
        telemetry = _telemetryServiceSharedInstance
        // create the bucket
        telemetry.createBucket()

    }

    func get_last_uv()->Float
    {
        return last_uv!
        
    }
    func uvExposure(uvIndex: Float, hours:Int) {
        let localNotification: UILocalNotification = UILocalNotification()
        localNotification.alertAction = "UV exposure reached"
        localNotification.alertBody = "UV increased to \(uvIndex)"
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 1)
        localNotification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
    }
    func updateLiveData(uvIndex:Float, lux:Float) {
        //add to the daily bucket
        let now:NSDate = NSDate()
        HistoricalData.sharedInstance.histogram24.Bucketize(now,value:Double(uvIndex))
        //check if the Uv index has escalated from the previous hour
        let now_hour = getCurrentHour().hour
        if (now_hour>1)
        {
            last_uv = Float(HistoricalData.sharedInstance.histogram24.GetValue(now_hour - 1))
            
            if (uvIndex > last_uv_alert && uvIndex >= (UserSetting.sharedInstance.getMaxUv()!-0.1))
            {
                print("Uv level increased")
                print("Last time alert ")
                print(last_time_alert)
                let tdiff = timeDiff(last_time_alert!, date2: now)
                if (tdiff.hour > 1)
                {
                    print("Producing notification for the first time")
                    uvExposure(uvIndex, hours: 1)
                    last_time_alert = now
                    last_uv_alert = uvIndex
                }
                else{
                    print("Previously notified")
                }
            }
            
        }
        if let delegate = self.delegate {
            delegate.liveData(uvIndex, lux: lux)
        }
        //save the last reading always
        last_uv = uvIndex
        
        //send to telemetry
        telemetry.sendSingleData(last_time_alert!,value: last_uv!,stream: "uvindex")

    }
    
}

class ProximityDataManager
{
    var delegate:ProximityDataManagerDelegate?
    var lastProx: Int = 150
    
    class var sharedInstance: ProximityDataManager {
        return _ProximityDataManagerSharedInstance
    }
    
    init() {
        print("Proximity Data Manager Created")
    }
    
    func updateLiveData(rssi:Int) {
        lastProx = rssi
        if let delegate = self.delegate {
            delegate.liveData(rssi)
        }
    }
    
    func disconnected() {
        if let delegate = self.delegate {
            delegate.disconnected()
        }
    }
    
}