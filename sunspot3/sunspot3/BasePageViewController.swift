//
//  BasePageViewController.swift
//  sunspot3
//
//  Created by Janek Mann on 25/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit

class BasePageViewController: UIViewController {
    var pageIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        let topColour = UIColor(red: 1.0, green: (204/255), blue: 0, alpha: 1.0)
        let bottomColour = UIColor(red: 1.0, green: (160/255.0), blue: 0, alpha: 1.0)
        configureGradientBackground(topColour.CGColor, bottomColour.CGColor)
    }
    func updateDelegates(viewController: UIViewController) {
        if (viewController is UVDataManagerDelegate) {
            UVDataManager.sharedInstance.delegate = (viewController as? UVDataManagerDelegate)
        } else {
            UVDataManager.sharedInstance.delegate = nil
        }
        if (viewController is ProximityDataManagerDelegate) {
            ProximityDataManager.sharedInstance.delegate = (viewController as? ProximityDataManagerDelegate)
        } else {
            ProximityDataManager.sharedInstance.delegate = nil
        }
        
    }

    func configureGradientBackground(colors:CGColorRef...){
        
        let gradient: CAGradientLayer = CAGradientLayer()
        let maxWidth = max(self.view.bounds.size.height,self.view.bounds.size.width)
        let squareFrame = CGRect(origin: self.view.bounds.origin, size: CGSizeMake(maxWidth, maxWidth))
        gradient.frame = squareFrame
        
        gradient.colors = [SunSpotStyleKit.backgroundGradientColor.CGColor, SunSpotStyleKit.backgroundGradientColor2.CGColor]
        view.layer.insertSublayer(gradient, atIndex: 0)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        updateDelegates(self as UIViewController)
    }
}