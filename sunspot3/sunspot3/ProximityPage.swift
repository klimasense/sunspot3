//
//  ProximityPage.swift
//  sunspot3
//
//  Created by Janek Mann on 25/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit

class ProximityPage: BasePageViewController, ProximityDataManagerDelegate {
    @IBOutlet var proximityView: ProximityView!
    @IBOutlet var summary: UILabel!
    
    var last: NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        proximityView.proxIndex = 3
        summary.text = "SunSpot disconnected"
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        ProximityDataManager.sharedInstance.delegate = self

        
    }
    func disconnected() {
        summary.text = "SunSpot disconnected"
        proximityView.proxIndex = 3
    }
    func liveData(rssi: Int) {
        if (rssi > -60) {
            proximityView.proxIndex = 2
            summary.text = "SunSpot close"
        } else if (rssi > -80) {
            proximityView.proxIndex = 1
            summary.text = "SunSpot nearby"
        } else {
            proximityView.proxIndex = 0
            summary.text = "SunSpot seen"
        }
    }
}