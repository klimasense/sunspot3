//
//  TelemetryInitialState.swift
//  sunspot3
//
//  Created by Paolo Di Prodi on 21/05/2016.
//  Copyright © 2016 spotsensors. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Alamofire_Synchronous

public let _telemetryServiceSharedInstance = InitialState()

public class InitialState {

    var lastDate: NSDate = NSDate(timeIntervalSince1970: 0)
    var key: String = "TQbGI0qymTF1NA8x8YVMfeIMfzJMyXwQ"
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var deviceid:String = UIDevice.currentDevice().identifierForVendor!.UUIDString
    var appid:String = NSUUID().UUIDString
    
    var enabled:Bool = false
    
    public init() {
        print(" Initial State launched")
        
        //Retrieve
        print(deviceid)
        print(appid)

    }
    class var sharedInstance: InitialState {
        return _telemetryServiceSharedInstance
    }

    public func sendSingleData(timestamp:NSDate,value:Float,stream:String)
    {
        if(self.enabled)
        {
            let url = "https://groker.initialstate.com/api/events"

            var points = [[String:String]]()
            points.append(["key":stream,"value":"\(value)"])
            
            let request = NSMutableURLRequest(URL: NSURL(string:url)!)
            request.HTTPMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(self.key, forHTTPHeaderField: "X-IS-AccessKey")
            request.setValue(self.deviceid, forHTTPHeaderField: "X-IS-BucketKey")
            
            do {
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(points, options: NSJSONWritingOptions.PrettyPrinted)

            } catch let error as NSError {
                print(error)
            }
        
            let response = Alamofire.request(request).validate().responseJSON()

            if let json:JSON = JSON(data : response.data!)  {
                print(json["code"])
                print(json["message"])
            }
        }

    }
    public func createBucket()
    {
        let url = "https://groker.initialstate.com/api/buckets"
        let params = ["bucketKey":self.deviceid,"bucketName":self.appid,"accessKey":self.key]
        
        let response = Alamofire.request(.POST, url, parameters: params).validate().responseJSON()
        
        switch response.result {
        case .Success:
            self.enabled = true
            if let json:JSON = JSON(data : response.data!)  {
                self.enabled=true
            }
        case .Failure(let error):
            print(error)
            self.enabled = false
        }
    
        
    }
}
