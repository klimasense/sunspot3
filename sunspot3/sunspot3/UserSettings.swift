//
//  UserSettings.swift
//  sunspot
//
//  Created by Paolo Di Prodi on 01/05/2015.
//  Copyright (c) 2015 spotsensors. All rights reserved.
//

import Foundation

private let _UserSettingSharedInstance = UserSetting()
private let _HistoricalDataSharedInstance = HistoricalData()
private let _WorldWeatherOnlineSharedInstance = WorldWeatherOnline()

class HistoricalData
{
    var defaults: NSUserDefaults?
    var histogram24: BucketizeDay
    
    class var sharedInstance: HistoricalData {
        return _HistoricalDataSharedInstance
    }
    
    init() {
        print("Historical data initiated")
        defaults = NSUserDefaults.standardUserDefaults()
        histogram24 = BucketizeDay(samplesinday: 24)
        //check if we have an existing histogram for today
        getHistogram(histogram24.currentDate)
    }
    
    func saveHistogram() {
        
        defaults?.setObject(histogram24, forKey:histogram24.currentDate)
    }
    
    
    func getHistogram(datestamp:String) {
        if defaults?.arrayForKey(datestamp) != nil
        {
            histogram24.buckets = (defaults?.arrayForKey(datestamp) as! Array<Double>)
        }
    }
    
    func returnHistogram(datestamp:String)->Array<Float> {
        if defaults?.arrayForKey(datestamp) != nil
        {
            return (defaults?.arrayForKey(datestamp) as? Array<Float>)!
        }
        else{
          return Array<Float>()
        }
    }
    
}

class WorldWeatherOnline
{
    var defaults: NSUserDefaults?
    // premium key will expire in 60 days from 6 May 2016
    var key:String = "7330e54e7f404de197e65235160605"
    
    class var sharedInstance: WorldWeatherOnline {
        return _WorldWeatherOnlineSharedInstance
    }
    
    init() {
        print("World Weather settings initiated")
        defaults = NSUserDefaults.standardUserDefaults()
    }
    
    func saveKey(text:String?) {
        
        if text != nil
        {
            key = text!
        }
        
        defaults?.setObject(key, forKey: "wwo_key")
        
    }
    
    func getKey()->String? {
        if let key:String = defaults?.stringForKey("wwo_key")
        {
            self.key = key
            return key
        }
        else
        {

            return self.key
        }
        
    }
}
class UserSetting
{
    var defaults: NSUserDefaults?
    var username:String?
    var deviceid:String?
    var skintype:Int?
    var maxUv:Float?

    class var sharedInstance: UserSetting {
        return _UserSettingSharedInstance
    }
    
    init() {
        print("User setting initiated")
        defaults = NSUserDefaults.standardUserDefaults()
        maxUv = 10.0
    }
    
    func saveUsername(text:String?) {
        
        if text != nil
        {
            username = text

        }
        
        defaults?.setObject(username, forKey: "username")
        
    }
    func saveDevice(text:String?) {
        if text != nil
        {
            deviceid = text
        }
        defaults?.setObject(deviceid, forKey: "device")
    }
    func saveSkinType(text:Int?) {
        if text != nil
        {
            skintype=text
        }
        defaults?.setObject(skintype, forKey: "skintype")
    }
    
    func getMaxUv() -> Float?{
        if ( self.maxUv == nil )
        {    return 0.0 }
        else
        {
            return self.maxUv
        }
    }
    
    func setMaxUv(value:Float)
    {
        self.maxUv = value
    
    }
    func getUsername()->String? {
        if let username:String = defaults?.stringForKey("username")
        {
            self.username = username
            return username
        }
        else
        {
            self.username="gordon"
            return self.username
        }
        
    }
    func getDevice()->String? {
        if let deviceid:String = defaults?.stringForKey("device")
        {
            self.deviceid = deviceid
            return deviceid
        }
        else
        {
            self.deviceid = "SunSpot2"
            return self.deviceid
        }
    }
    func getSkinType()->Int? {
        if let skintype:Int = defaults?.integerForKey("skintype")
        {
            self.skintype=skintype
            return skintype
        }
        else
        {
            self.skintype = 1
            return self.skintype
        }
    }
    
}
