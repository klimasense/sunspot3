//
//  InfoPage.swift
//  sunspot3
//
//  Created by Janek Mann on 10/09/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class InfoPage: BasePageViewController {
    @IBOutlet var webView: UIWebView!
    @IBAction func doneButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {})
    }
    func pathForBuggyWKWebView(filePath: String?) -> String? {
        let fileMgr = NSFileManager.defaultManager()
        let path = NSTemporaryDirectory() + "/www"
        do {
            try fileMgr.createDirectoryAtPath(path, withIntermediateDirectories: true, attributes: nil);
        } catch {
            print("Couldn't create www subdirectory. \(error)")
            return nil
        }
        let dstPath = path + "/" + NSString(string: filePath!).lastPathComponent
        if !fileMgr.fileExistsAtPath(dstPath) {
            do {
                
                try  fileMgr.copyItemAtPath(filePath!, toPath: dstPath);
            } catch {
                print("Couldn't copy file to /tmp/www. \(error)")
                return nil
            }
            
        }
        return dstPath
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var filePath = NSBundle.mainBundle().pathForResource("sunsmart", ofType: "pdf")
        filePath = pathForBuggyWKWebView(filePath) // This is the reason of this entire thread!
        let request = NSURLRequest(URL: NSURL.fileURLWithPath(filePath!))
        self.webView!.loadRequest(request)
    }
}