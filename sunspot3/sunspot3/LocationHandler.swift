//
//  LocationHandler.swift
//  sunspot3
//
//  Created by Paolo Di Prodi on 23/09/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//


import UIKit
import CoreLocation

/**
* Custom protocol to share the location, speed and heading information with other classes that implements this protocol
*/

@objc protocol LocationHandlerProtocol {
    /**
    * Returns the speed and the current location objects
    * @param: speed - current speed of the user
    * @param: location - current location of the user
    */
    func locationHandlerDidUpdateLocationWithSpeed(speed: Double, location: CLLocation)
    
    /**
    * Returns the current location objecgt
    * @param: location - current location of the user
    */
    func locationHandlerDidUpdateLocation(location: CLLocation)
    
    /**
    * Return the current heading direction of the user
    * @param: newHeading - the new heading data to set the compass' direction
    */
    optional func locationHandlerDidUpdateHeading(newHeading: CLHeading)
}

/**
* Custom object to handle location and heading events.
*/

class LocationHandler: NSObject, CLLocationManagerDelegate {
    
    var locationHandlerProtocol: LocationHandlerProtocol?
    var locationManager: CLLocationManager!
    var currentUserLocation: CLLocation?
    
    /**
    * Initializer of the class.
    */
    
    /**
    * Sets up the location manager and its properties
    */
    private func setupLocationHandler() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        //locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.distanceFilter = 50.0
        //locationManager.headingFilter = kCLHeadingFilterNone
    }
    
    func requestLocation()
    {
        locationManager.requestLocation()
        
    }
    func getLastLocation()->CLLocation
    {
        
        return currentUserLocation!
    }
    
    /**
    * Starts the location tracking
    */
    func startLocationTracking() {
        locationManager = CLLocationManager()
        setupLocationHandler()
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            locationManager.requestWhenInUseAuthorization()
        }  
        //locationManager.startUpdatingHeading()
    }
    
    /**
    * Tells the delegate that the location manager received updated heading information.
    * @param: manager - The location manager object that generated the update event.
    * @param: newHeading
    */
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        locationHandlerProtocol?.locationHandlerDidUpdateHeading?(newHeading)
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error finding location: \(error.localizedDescription)")
    }
    
    
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if status == .AuthorizedAlways || status == .AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    /**
    * Tells the delegate that new location data is available. If the speed is bigger than zero, call the protocol method to let
    * other classes know of the new data available
    * @param: manager - The location manager object that generated the update event.
    * @param: locations - An array of CLLocation objects containing the location data
    */
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let firstLocation = locations.first as CLLocation!
        {
            let speed = firstLocation.speed
            if speed > 0 {
                locationHandlerProtocol?.locationHandlerDidUpdateLocationWithSpeed(speed , location: firstLocation)
            }
            else
            {
                locationHandlerProtocol?.locationHandlerDidUpdateLocation(firstLocation)
            }
        }
        currentUserLocation = locations.first as CLLocation!
    }
    
    /**
    * Asks the delegate whether the heading calibration alert should be displayed.
    * @param: manager - The location manager object coordinating the display of the heading calibration alert.
    * @return: Bool - Yes, if it should display the calibration
    */
    func locationManagerShouldDisplayHeadingCalibration(manager: CLLocationManager) -> Bool {
        return false
    }
    
}

