//
//  SettingsPage.swift
//  sunspot3
//
//  Created by Janek Mann on 25/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class SettingsPage: BasePageViewController {
    @IBOutlet var SkinType: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoadSettings()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        LoadSettings()
    }
    
    func LoadSettings() {
        print("Loaded skin type \(UserSetting.sharedInstance.getSkinType())")
        //self.DeviceID.text=UserSetting.sharedInstance.getDevice()
        //self.Username.text=UserSetting.sharedInstance.getUsername()
        self.SkinType.selectedSegmentIndex = UserSetting.sharedInstance.getSkinType()!-1
    }
    
    @IBAction func SaveDataEvent(sender: AnyObject) {
        
        //UserSetting.sharedInstance.saveUsername(self.Username.text)
        //UserSetting.sharedInstance.saveDevice(self.DeviceID.text)
        print("Changed skin type ")
        UserSetting.sharedInstance.saveSkinType(self.SkinType.selectedSegmentIndex+1)
        
    }
    
}