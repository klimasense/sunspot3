//
//  UVIDialView.swift
//  sunspot3
//
//  Created by Janek Mann on 26/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
class HistogramView: UIView {
    @IBInspectable var bar1Height: CGFloat = 0.5 {
        // Update your UI when value changes
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var bar2Height: CGFloat = 0.75 {
        // Update your UI when value changes
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var bar3Height: CGFloat = 0.75 {
        // Update your UI when value changes
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var bar4Height: CGFloat = 0.75 {
        // Update your UI when value changes
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var bar5Height: CGFloat = 0.75 {
        // Update your UI when value changes
        didSet {
            self.setNeedsDisplay()
        }
    }
    override func drawRect(rect: CGRect) {
        let frame1 = self.frame.insetBy(dx: 0, dy: 0)
        let context = UIGraphicsGetCurrentContext()
        CGContextSaveGState(context)
        CGContextScaleCTM (context, frame1.width/240, frame1.height/240)
        
        SunSpotStyleKit.drawHistogram(bar1Height, bin2Scale: bar2Height, bin3Scale: bar3Height, bin4Scale: bar4Height, bin5Scale: bar5Height)
        CGContextRestoreGState(context)
    }
}