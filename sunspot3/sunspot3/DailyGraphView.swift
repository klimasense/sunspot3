//
//  UVIDialView.swift
//  sunspot3
//
//  Created by Janek Mann on 26/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable
class DailyGraphView: UIView {
    var xIndices = ["Mon", "Tue"]
    var values:[Double] = [0.9, 0.5, 0.9, 1.0, 2.8, 3.0, 1.5] {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var uVI: CGFloat = 7.5 {
        // Update your UI when value changes
        didSet {
            self.setNeedsDisplay()
        }
    }
    override func drawRect(rect: CGRect) {
        let frame1 = self.frame.insetBy(dx: 0, dy: 0)
        let context = UIGraphicsGetCurrentContext()
        CGContextSaveGState(context)
        CGContextScaleCTM (context, frame1.width/240, frame1.height/240)
        drawGraph(values, max: 15.0)
        CGContextRestoreGState(context)
    }
    private func drawGraph(values: [Double], max: Double) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()
        var pointSpacing:Double = 0.0
        if ( values.count>1 ){
            pointSpacing = 220.0 / ( Double(values.count) - 1)
        }
        else
        {
            pointSpacing = 220.0 / ( Double(values.count))
        }
    
        let scaling = 220.0 / max
        
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPointMake(10, 230))
        for (index, value) in values.enumerate() {
            //CGPointMake(0.1, 0.1)
            bezierPath.addLineToPoint(CGPointMake(CGFloat(index) * CGFloat(pointSpacing) + 10, CGFloat(230.0) - CGFloat(value * scaling)))
        }
        bezierPath.addLineToPoint(CGPointMake(230, 230))
        bezierPath.closePath()
        CGContextSaveGState(context)
        bezierPath.addClip()
        CGContextDrawLinearGradient(context, SunSpotStyleKit.graphGradient, CGPointMake(0, 240), CGPointMake(0, 50), CGGradientDrawingOptions.DrawsAfterEndLocation)
        CGContextRestoreGState(context)
        
        let linePath = UIBezierPath()
        linePath.moveToPoint(CGPointMake(10, CGFloat(230.0) - CGFloat(values[0] * scaling)))
        for (index, value) in values.enumerate() {
            //CGPointMake(0.1, 0.1)
            linePath.addLineToPoint(CGPointMake(CGFloat(index) * CGFloat(pointSpacing) + 10, CGFloat(230.0) - CGFloat(value * scaling)))
            //// Oval Drawing
            let ovalPath = UIBezierPath(ovalInRect: CGRectMake(CGFloat(index) * CGFloat(pointSpacing) - CGFloat(2) + 10, CGFloat(230.0) - CGFloat(value * scaling + 2), 4, 4))
            UIColor.whiteColor().setFill()
            ovalPath.fill()
        }
        UIColor.whiteColor().setStroke()
        linePath.lineWidth = 1
        linePath.stroke()
        for (index, value) in [3.0,6.0,9.0].enumerate() {
            //// Text Drawing
            let textRect = CGRectMake(-10, CGFloat(220.0 - (value * scaling) + 6), 20, 12)
            let textTextContent = NSString(string: "\(Int(value))")
            let textStyle = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
            textStyle.alignment = NSTextAlignment.Right
            
            let textFontAttributes = [NSFontAttributeName: UIFont.systemFontOfSize(8), NSForegroundColorAttributeName: UIColor.whiteColor(), NSParagraphStyleAttributeName: textStyle]
            
            let textTextHeight: CGFloat = textTextContent.boundingRectWithSize(CGSizeMake(textRect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: textFontAttributes, context: nil).size.height
            CGContextSaveGState(context)
            CGContextClipToRect(context, textRect);
            textTextContent.drawInRect(CGRectMake(textRect.minX, textRect.minY + (textRect.height - textTextHeight) / 2, textRect.width, textTextHeight), withAttributes: textFontAttributes)
            CGContextRestoreGState(context)
        }
        for (index, value) in self.xIndices.enumerate() {
            //// Text Drawing
            let textRect = CGRectMake(CGFloat(Double(index) * pointSpacing), 230, 20, 12)
            let textTextContent = NSString(string: "\(value)")
            let textStyle = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
            textStyle.alignment = NSTextAlignment.Center
            
            let textFontAttributes = [NSFontAttributeName: UIFont.systemFontOfSize(8), NSForegroundColorAttributeName: UIColor.whiteColor(), NSParagraphStyleAttributeName: textStyle]
            
            let textTextHeight: CGFloat = textTextContent.boundingRectWithSize(CGSizeMake(textRect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: textFontAttributes, context: nil).size.height
            CGContextSaveGState(context)
            CGContextClipToRect(context, textRect);
            textTextContent.drawInRect(CGRectMake(textRect.minX, textRect.minY + (textRect.height - textTextHeight) / 2, textRect.width, textTextHeight), withAttributes: textFontAttributes)
            CGContextRestoreGState(context)
        }

    }
}

