//
//  UVIDialView.swift
//  sunspot3
//
//  Created by Janek Mann on 26/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

@IBDesignable
class ProximityView: UIView {
    @IBInspectable var blurRadius: Double = 15.0 {
        // Update your UI when value changes
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var proxIndex: Int = 2 {
        // Update your UI when value changes
        didSet {
            self.setNeedsDisplay()
        }
    }
    var updater: CADisplayLink!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //updater = CADisplayLink(target: self, selector: Selector("animateView"))
        //updater.frameInterval = 1
        //updater.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSRunLoopCommonModes)
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updater = CADisplayLink(target: self, selector: Selector("animateView"))
        updater.frameInterval = 4
        updater.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSRunLoopCommonModes)
        //super.init(aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    override func drawRect(rect: CGRect) {
        let frame1 = self.frame.insetBy(dx: 0, dy: 0)
        let context = UIGraphicsGetCurrentContext()
        CGContextSaveGState(context)
        CGContextScaleCTM (context, frame1.width/240, frame1.height/240)
        drawProximity(blurRadius, index: proxIndex)
        CGContextRestoreGState(context)
    }
    
    func animateView() {
        blurRadius = sin(updater.timestamp * 5.0) * 6 + 5
        self.setNeedsDisplay()
    }
    
    private func drawProximity(blurRadius: Double, index: Int) {
        let context = UIGraphicsGetCurrentContext()
        
        
        //// Shadow Declarations
        //let shadow2 = NSShadow()
        //shadow2.shadowColor = UIColor.whiteColor()
        //shadow2.shadowOffset = CGSizeMake(0, 0)
        //shadow2.shadowBlurRadius = CGFloat(blurRadius)
        if (index == 0) {
            CGContextDrawRadialGradient(context, SunSpotStyleKit.graphGradient, CGPointMake(120, 120), 90 + CGFloat(blurRadius), CGPointMake(120, 120), 90, CGGradientDrawingOptions(rawValue: 0))
        }
        //// Oval Drawing
        let ovalPath = UIBezierPath(ovalInRect: CGRectMake(30, 30, 180, 180))
        //CGContextSaveGState(context)
        //CGContextSetShadowWithColor(context, shadow2.shadowOffset, shadow2.shadowBlurRadius, (shadow2.shadowColor as! UIColor).CGColor)
        SunSpotStyleKit.proximityCol1.setFill()
        ovalPath.fill()
        //// Oval Drawing
        if (index == 1) {
            CGContextDrawRadialGradient(context, SunSpotStyleKit.graphGradient, CGPointMake(120, 120), 60 + CGFloat(blurRadius), CGPointMake(120, 120), 60, CGGradientDrawingOptions(rawValue: 0))
        }
        let ovalPath2 = UIBezierPath(ovalInRect: CGRectMake(60, 60, 120, 120))
        //CGContextSaveGState(context)
        //CGContextSetShadowWithColor(context, shadow2.shadowOffset, shadow2.shadowBlurRadius, (shadow2.shadowColor as! UIColor).CGColor)
        SunSpotStyleKit.proximityCol2.setFill()
        ovalPath2.fill()
        //// Oval Drawing
        if (index == 2) {
            CGContextDrawRadialGradient(context, SunSpotStyleKit.graphGradient, CGPointMake(120, 120), 30 + CGFloat(blurRadius), CGPointMake(120, 120), 30, CGGradientDrawingOptions(rawValue: 0))
        }
        let ovalPath3 = UIBezierPath(ovalInRect: CGRectMake(90, 90, 60 , 60))
        //CGContextSaveGState(context)
        //CGContextSetShadowWithColor(context, shadow2.shadowOffset, shadow2.shadowBlurRadius, (shadow2.shadowColor as! UIColor).CGColor)
        SunSpotStyleKit.proximityCol3.setFill()
        ovalPath3.fill()
        //CGContextRestoreGState(context)
    }
}