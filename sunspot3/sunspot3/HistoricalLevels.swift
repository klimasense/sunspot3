//
//  HistoricalLevels.swift
//  sunspot3
//
//  Created by Paolo Di Prodi on 23/09/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation

//internally intervals are expressed in seconds
public class HistoricalLevels {
    // level bins
    var intervals: Int = 5
    var levels: Array<Float>
    var today: NSDate
    //initialize the histogram array of counts with at least 24 hours time span
    // the structure is like bucket[hour]=max(UVindex)
    init(today:NSDate=NSDate())
    {
        self.levels = Array<Float>(count: self.intervals, repeatedValue: 0)
        self.today = today
    }
    
    func compute(past:Int=7)
    {
    
        for var index = 0; index < past; ++index
        {
            //load histograms of past dates
            var dayComponenet = NSDateComponents()
            dayComponenet.day = -1
            
            let theCalendar = NSCalendar.currentCalendar()
            let prevDate = theCalendar.dateByAddingComponents(dayComponenet, toDate: today,options:NSCalendarOptions.init(rawValue: 0))
            
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let currentDate = dateFormatter.stringFromDate(prevDate!)
            
            //now load the histogram for that specific day
            var buckets:Array<Float> = HistoricalData.sharedInstance.returnHistogram(currentDate)
            if ( buckets.count > 0 ){
                //print("Day \(currentDate) is full")
            }
            //now count how many hours are for each level
            for var hour = 0; hour<buckets.count ; ++hour
            {
                let uv = buckets[hour]
                if (  uv >= 0 && uv < 2.9)
                {
                    self.levels[0] += 1
                }
                else if ( uv >= 2.9 && uv < 3.9)
                {
                    self.levels[1] += 1
                    
                }
                else if ( uv >= 4.0 && uv < 5.9)
                {
                    
                    self.levels[2] += 1
                }
                else if ( uv >= 6.0 && uv < 8.9)
                {
                    
                    self.levels[3] += 1
                }
                else if ( uv >= 9)
                {
                    
                    self.levels[4] += 1
                }
            }
        }
    }
    
    func get_levels()->Array<Float>
    {
        return self.levels
    }
}

