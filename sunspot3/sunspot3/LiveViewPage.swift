//
//  LiveViewPage.swift
//  sunspot3
//
//  Created by Janek Mann on 25/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class LiveViewPage: BasePageViewController,UVDataManagerDelegate,LocationHandlerProtocol {
    var currentUVI:CGFloat = 0.0
    @IBOutlet var uvRiskMessage: UILabel!
    @IBOutlet var uVIDial: uVIDialView!
    @IBOutlet var uviLabel: UILabel!
    let locationHandler = LocationHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uVIDial.uVI = currentUVI
        uVIDial.pulseActive = false
        uvRiskMessage.text = "Not Connected"
        
        locationHandler.startLocationTracking()
        locationHandler.locationHandlerProtocol = self
        locationHandler.requestLocation()
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    func liveData(uvIndex: Float, lux: Float) {
        currentUVI = CGFloat(uvIndex)
        uVIDial.uVI = currentUVI
        if (currentUVI <= 0.01) {
            uVIDial.uVI = 0.1
        }
        uviLabel.text = "Current UVI: " + String(NSString(format: "%.2f", currentUVI))
        uvRiskMessage.text = uvTextAlert(uvIndex,skintype: UserSetting.sharedInstance.getSkinType()!)
        
    }
    
    @objc func locationHandlerDidUpdateLocation(location: CLLocation) {
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            print(location)
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0] as! CLPlacemark
                print(pm.locality)
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    @objc func locationHandlerDidUpdateLocationWithSpeed(speed: Double, location: CLLocation) {
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            print(location)
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0] as! CLPlacemark
                print(pm.locality)
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    
    func uvTextAlert(uvIndex:Float,skintype:Int)->String
    {
        //decide what color to display
        
        if( uvIndex>=0 && uvIndex<=2.9)
        {
            
            self.uvRiskMessage.text = "Low risk"
            
        }
        else if( uvIndex>2.9 && uvIndex<=4.9)
        {
            if(skintype<=2)
            {    self.uvRiskMessage.text = "Medium risk"}
            else
            {    self.uvRiskMessage.text = "Low risk"}
            
        }
        else if( uvIndex>5.0 && uvIndex<=5.9)
        {
            if skintype<=2
            {    self.uvRiskMessage.text = "High Risk"}
            else if skintype <= 4
            {    self.uvRiskMessage.text = "Medium Risk"}
            else
            {    self.uvRiskMessage.text = "Low Risk"}
            
        }
        else if( uvIndex>6.0 && uvIndex<=6.9)
        {
            if skintype<=2
            {    self.uvRiskMessage.text = "High Risk"}
            else if skintype <= 5
            {    self.uvRiskMessage.text = "Medium Risk"}
            else
            {    self.uvRiskMessage.text = "Low Risk"}
        }
        else if( uvIndex>7.0)
        {
            if skintype<=2
            {    self.uvRiskMessage.text = "Very High Risk"}
            else if skintype <= 5
            {    self.uvRiskMessage.text = "High Risk"}
            else
            {    self.uvRiskMessage.text = "Medium Risk"}
        }
        
        return self.uvRiskMessage.text!

        
    }
}