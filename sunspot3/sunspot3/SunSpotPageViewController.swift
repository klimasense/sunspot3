//
//  MyPageViewController.swift
//  sunspot3
//
//  Created by Janek Mann on 25/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit

class SunSpotPageViewController: UIPageViewController, UIPageViewControllerDataSource {
    var pageIdentifiers = ["liveView", "histogramView", "graphView", "proximityView", "settingsPage"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        let view1 = self.storyboard!.instantiateViewControllerWithIdentifier(pageIdentifiers[0])

        self.setViewControllers([view1], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        //self.bleManager.start()
        self.view.backgroundColor = SunSpotStyleKit.backgroundGradientColor2
        let page = UIPageControl.appearance()
        page.backgroundColor = SunSpotStyleKit.backgroundGradientColor2
        page.pageIndicatorTintColor = SunSpotStyleKit.backgroundGradientColor
        page.currentPageIndicatorTintColor = UIColor.whiteColor()
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.bleManager.start()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        WeatherService.sharedInstance.start_prediction()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let _viewController = viewController as! BasePageViewController
        return getViewController(_viewController.pageIndex + 1)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let _viewController = viewController as! BasePageViewController
        return getViewController(_viewController.pageIndex - 1)
    }
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return pageIdentifiers.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    private func getViewController(itemIndex: Int) -> BasePageViewController? {
        
        if (itemIndex >= 0 && itemIndex < pageIdentifiers.count) {
            let viewController = self.storyboard!.instantiateViewControllerWithIdentifier(pageIdentifiers[itemIndex]) as! BasePageViewController
            viewController.pageIndex = itemIndex
            return viewController
        } else {
            return Optional.None
        }
    }

}