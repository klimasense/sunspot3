//
//  LiveViewPage.swift
//  sunspot3
//
//  Created by Janek Mann on 25/08/2015.
//  Copyright © 2015 spotsensors. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    var CGFloatValue: CGFloat {
        get {
            return CGFloat(self)
        }
    }
}
extension Int {
    var CGFloatValue: CGFloat {
        get {
            return CGFloat(self)
        }
    }
}
extension Float {
    var CGFloatValue: CGFloat {
        get {
            return CGFloat(self)
        }
    }
}

class HistogramPage: BasePageViewController {
    @IBOutlet var bars: HistogramView!
    @IBOutlet var summary: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let history : HistoricalLevels = HistoricalLevels()
        history.compute()
        var alldata: Array<Float> = history.get_levels()
        
        //set all bars individually
        bars.bar1Height = min(alldata[0].CGFloatValue/(7*24.0),0.8)
        bars.bar2Height = min(alldata[1].CGFloatValue/(7*24.0),0.8)
        bars.bar3Height = min(alldata[2].CGFloatValue/(7*24.0),0.8)
        bars.bar4Height = min(alldata[3].CGFloatValue/(7*24.0),0.8)
        bars.bar5Height = min(alldata[4].CGFloatValue/(7*24.0),0.8)
        
        //write a summary
        let sum = alldata.reduce(0, combine: +)
        if (sum<=0)
        {
            summary.text = "Low exposure"
        }
        else
        {
            
            if ( bars.bar5Height > 0.0)
            {
                summary.text = "Peak hours \(bars.bar5Height)"
            }
            else if( bars.bar4Height > 0.0)
            {
                summary.text = "Peak hours \(bars.bar4Height)"
            }
            else if( bars.bar3Height > 0.0)
            {
                summary.text = "Peak hours \(bars.bar3Height)"
            }
            else if( bars.bar2Height > 0.0)
            {
                summary.text = "Peak hours \(bars.bar2Height)"
            }
            else if( bars.bar1Height > 0.0)
            {
                summary.text = "Peak hours \(bars.bar1Height)"
            }
        }
    }
}
